import { useContext } from 'react';
import { AppContext } from '../../../context';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styles from './index.styl';

const Sidebar = () => {
  const router = useRouter();
  const { state: notes, dispatch } = useContext(AppContext);

  return (
    <aside className={styles.sidebar}>
      <h3>Notes</h3>
      <ul>
        {notes &&
          notes.map((note, index) => (
            <li
              className={router.query.id === (index + 1).toString() ? styles.active : ''}
              key={index}>
              <Link href={`/${index + 1}`}>
                <a>{`Note ${index + 1}`}</a>
              </Link>
            </li>
          ))}
      </ul>
    </aside>
  );
};

export default Sidebar;
