import { useContext } from 'react';
import { useRouter } from 'next/router';
import { AppContext } from '../../../context';
import { FiPlusCircle } from 'react-icons/fi';
import styles from './index.styl';

const Header = () => {
  const router = useRouter();
  const { state, dispatch } = useContext(AppContext);

  const handleAdd = () => {
    dispatch({ type: 'add', payload: { text: '' } });
  };

  return (
    <header className={styles.header}>
      <h1>Note {router.query.id}</h1>
      <div className={styles.actions}>
        <button type="button" onClick={handleAdd}>
          <FiPlusCircle />
        </button>
      </div>
    </header>
  );
};

export default Header;
