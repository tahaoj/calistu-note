import Header from '../header';
import Sidebar from '../sidebar';
import styles from './index.styl';

const Layout = ({ children }) => {
  return (
    <div className={styles.layout}>
      <Sidebar />
      <div className={styles.main}>
        <Header />
        {children}
      </div>
    </div>
  );
};

export default Layout;
