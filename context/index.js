import { useRouter } from 'next/router';
import { useContext, useReducer } from 'react';

export const AppContext = React.createContext({});

const Context = ({ children }) => {
  const router = useRouter();
  const initialState =
    typeof localStorage !== 'undefined' ? JSON.parse(localStorage.getItem('notes') || '[]') : [];

  const reducer = (state, action) => {
    switch (action.type) {
      case 'add':
        return [...state, { ...action.payload }];
      case 'save':
        let tempState = state;
        tempState = tempState.map((note, index) => {
          if (index + 1 === parseInt(action.payload.index, 10)) note.text = action.payload.text;
          return note;
        });
        localStorage.setItem('notes', JSON.stringify(tempState));
        return [...tempState];
      case 'remove':
        let temp = state;
        temp = temp.filter((note, index) => (index + 1).toString() !== action.payload);
        localStorage.setItem('notes', JSON.stringify(temp));
        return [...temp];
    }
  };

  const [state, dispatch] = useReducer(reducer, initialState);
  const value = { state, dispatch };

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
};

export default Context;
