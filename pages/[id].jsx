import { useContext, useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { AppContext } from '../context';
import { FiCheck, FiTrash2 } from 'react-icons/fi';
import styles from './index.styl';

const HomePage = () => {
  const router = useRouter();
  const { state, dispatch } = useContext(AppContext);
  const [text, setText] = useState('');

  const handleSave = () => {
    dispatch({ type: 'save', payload: { index: router.query.id, text } });
  };

  const handleRemove = () => {
    dispatch({ type: 'remove', payload: router.query.id });
    router.push('/');
  };

  useEffect(() => {
    if (router) {
      if (state.length > 0)
        state.forEach((note, index) => {
          if ((index + 1).toString() === router.query.id) setText(note.text);
        });
      else router.push('/');
    }
  }, [router]);

  return (
    <main className={styles.homePage}>
      <div className={styles.editor}>
        <textarea value={text} onChange={(e) => setText(e.target.value)} />
      </div>

      <footer>
        <button type="button" onClick={handleSave}>
          <FiCheck />
          <span>Save</span>
        </button>

        <button type="button" onClick={handleRemove}>
          <FiTrash2 />
          <span>Remove</span>
        </button>
      </footer>
    </main>
  );
};

export default HomePage;
