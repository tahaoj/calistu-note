import Context from '../context';
import Layout from '../components/ui/layout';

const MyApp = ({ Component, pageProps }) => {
  return (
    <Context>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Context>
  );
};

export default MyApp;
